﻿using Persons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComparerEx
{
    class Program
    {
        static void Main(string[] args)
        {

            Person[] persons = new Person[5];

            #region array data
            persons[0] = new Person
            {
                SetId = 13742,
                SetAge = 32,
                SetHeight = 1.75f,
                SetName = "Shaul"
            };
            persons[1] = new Person
            {
                SetId = 31332,
                SetAge = 18,
                SetHeight = 1.60f,
                SetName = "Lila"
            };
            persons[2] = new Person
            {
                SetId = 13334,
                SetAge = 20,
                SetHeight = 1.80f,
                SetName = "John"
            };
            persons[3] = new Person
            {
                SetId = 44,
                SetAge = 23,
                SetHeight = 1.78f,
                SetName = "Dan"
            };
            persons[4] = new Person
            {
                SetId = 41,
                SetAge = 50,
                SetHeight = 1.70f,
                SetName = "Poal"
            };
            #endregion

            Array.Sort(persons, Person.NameComparer);
            PrintPersonArray(persons);
            //OUTPUT: Sorting by ID and not by Name

            Array.Sort(persons, Person.IDComparer);
            PrintPersonArray(persons);
            //OUTPUT: Sorting by ID 

            Array.Sort(persons, Person.AgeComparer);
            PrintPersonArray(persons);
            //OUTPUT: Sorting by ID and not by Age

            Array.Sort(persons, Person.HeightComparer);
            PrintPersonArray(persons);
            //OUTPUT: Sorting by ID and not by height

            Array.Sort(persons);
            PrintPersonArray(persons);

            PersonCompareByName personCompareByName = new PersonCompareByName();
            var result = personCompareByName.Compare(persons[0], persons[1]);
            Console.WriteLine(result);


        }

        static void PrintPersonArray(Person[] persons)
        {
            foreach(var person in persons)
            {
                Console.WriteLine(person.ToString());
            }
        }

    }
}
