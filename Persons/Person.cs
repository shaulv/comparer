﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persons
{
    public class Person : IComparable<Person>
    {
        #region Properties
        public int Id { get; private set; }
        public int SetId
        {
            set { Id = value; }
        }

        public int Age { get; private set; }
        public int SetAge
        {
            set { Age = value; }
        }
        public float Height { get; private set; }
        public float SetHeight
        {
            set { Height = value; }
        }
        public string Name { get; private set; }
        public string SetName
        {
            set { Name = value; }
        }
        #endregion

        #region Other properties
        static readonly IComparer<Person> idComparer;
        static readonly IComparer<Person> ageComparer;
        static readonly IComparer<Person> heightComparer;
        static readonly IComparer<Person> nameComparer;
        public static IComparer<Person> IDComparer
        {
            get { return idComparer; }
        }

        public static IComparer<Person> AgeComparer { 
            get { return ageComparer; } 
        }
        public static IComparer<Person> HeightComparer { 
            get { return heightComparer; }
        }
        public static IComparer<Person> NameComparer { 
            get { return nameComparer; }
        }
        public static IComparer<Person> DefaultComparer { get; }
        #endregion

        #region Constructors
        public Person()
        {

        }
        public Person(int id, int age, string name, float height)
        {
            this.SetAge = age;
            this.SetId = id;
            this.SetName = name;
            this.SetHeight = height;
        }
        static Person()
        {
            idComparer = default;
            ageComparer = default;
            heightComparer = default;
            nameComparer = default;
        }
        #endregion

        #region Methods
        public int CompareTo(Person obj) => this.Id.CompareTo(obj.Id);

        public override string ToString() => $"ID: {this.Id}\nName: {this.Name}\nAge: {this.Age}\nHeight: {this.Height}\n";

        #endregion
    }
}
